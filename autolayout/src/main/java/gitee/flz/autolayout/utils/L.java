package gitee.flz.autolayout.utils;

import android.util.Log;

/**
 * Modified by flz on 22/2/2.
 */
public class L
{
    public static boolean debug = true;
    private static final String TAG = "AUTO_LAYOUT";

    public static void e(String msg)
    {
        if (debug)
        {
            Log.e(TAG, msg);
        }
    }


}
