package gitee.flz.autolayout.config;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import gitee.flz.autolayout.utils.L;
import gitee.flz.autolayout.utils.ScreenUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Modified by flz on 20/02/26.
 */
public class AutoLayoutConifg {

    private static final AutoLayoutConifg sIntance = new AutoLayoutConifg();

    private static final String KEY_DESIGN_WIDTH = "design_width";
    private static final String KEY_DESIGN_HEIGHT = "design_height";
    private static final String KEY_TYPE = "design_type";//width宽生效 height高生效 whole都生效 auto根据设置的宽高计算比例自动适配

    private int mScreenWidth;
    private int mScreenHeight;

    private int mDesignWidth;
    private int mDesignHeight;

    private double mConversion;

    private boolean useDeviceSize;


    private AutoLayoutConifg() {
    }

    public void checkParams() {
        if (mDesignHeight <= 0 || mDesignWidth <= 0) {
            throw new RuntimeException(
                    "you must set " + KEY_DESIGN_WIDTH + " and " + KEY_DESIGN_HEIGHT + "  in your manifest file.");
        }
    }

    public AutoLayoutConifg useDeviceSize() {
        useDeviceSize = true;
        return this;
    }

    public static AutoLayoutConifg getInstance() {
        return sIntance;
    }

    public int getScreenWidth() {
        return mScreenWidth;
    }

    public double getConversion() {
        return mConversion;
    }

    public int getScreenHeight() {
        return mScreenHeight;
    }

    public int getDesignWidth() {
        return mDesignWidth;
    }

    public int getDesignHeight() {
        return mDesignHeight;
    }

    public void init(Activity context) {
        int[] screenSize = ScreenUtils.getScreenSize(context, useDeviceSize);
        mScreenWidth = screenSize[0];
        mScreenHeight = screenSize[1];
        L.e(" screenWidth =" + mScreenWidth + " ,screenHeight = " + mScreenHeight);
        getMetaData(context);
    }

    public void init(Context context) {
        int[] screenSize = ScreenUtils.getScreenSize(context, useDeviceSize);
        mScreenWidth = screenSize[0];
        mScreenHeight = screenSize[1];
        L.e(" screenWidth =" + mScreenWidth + " ,screenHeight = " + mScreenHeight);
        getMetaData(context);
    }

    private void getMetaData(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = packageManager.getApplicationInfo(context
                    .getPackageName(), PackageManager.GET_META_DATA);
            if (applicationInfo.metaData != null) {
                String type = applicationInfo.metaData.getString(KEY_TYPE,"whole");
                switch (type) {
                    case "height"://以高度为标准计算转换比例
                        mDesignHeight = applicationInfo.metaData.getInt(KEY_DESIGN_HEIGHT, -1);
                        mConversion = new BigDecimal(mScreenHeight).divide(new BigDecimal(mDesignHeight), 10, RoundingMode.HALF_UP).doubleValue();
                        mDesignWidth = new BigDecimal(mScreenWidth).divide(new BigDecimal(mConversion), 10, RoundingMode.HALF_UP).intValue();
                        break;
                    case "width"://以宽度为标准计算转换比例
                        mDesignWidth = applicationInfo.metaData.getInt(KEY_DESIGN_WIDTH, -1);
                        mConversion = new BigDecimal(mScreenWidth).divide(new BigDecimal(mDesignWidth), 10, RoundingMode.HALF_UP).doubleValue();
                        mDesignHeight = new BigDecimal(mScreenHeight).divide(new BigDecimal(mConversion), 10, RoundingMode.HALF_UP).intValue();
                        break;
                    case "auto"://以设置的宽高和实际屏幕宽高进行计算，那个比例大用哪个
                        mDesignHeight = applicationInfo.metaData.getInt(KEY_DESIGN_HEIGHT, -1);
                        mDesignWidth = applicationInfo.metaData.getInt(KEY_DESIGN_WIDTH, -1);
                        double wConversion = new BigDecimal(mDesignWidth).divide(new BigDecimal(mScreenWidth), 10, RoundingMode.HALF_UP).doubleValue();
                        double hConversion = new BigDecimal(mDesignHeight).divide(new BigDecimal(mScreenHeight), 10, RoundingMode.HALF_UP).doubleValue();
                        if (wConversion > hConversion) {
                            mConversion = wConversion;
                            mDesignHeight = new BigDecimal(mScreenHeight).multiply(new BigDecimal(mConversion)).intValue();
                        } else {
                            mConversion = hConversion;
                            mDesignWidth = new BigDecimal(mScreenWidth).multiply(new BigDecimal(mConversion)).intValue();
                        }
                        break;
                    default://以设置的宽高做为控件像素宽高比例
                        mDesignWidth = applicationInfo.metaData.getInt(KEY_DESIGN_WIDTH, -1);
                        mDesignHeight = applicationInfo.metaData.getInt(KEY_DESIGN_HEIGHT, -1);
                        mConversion = 0.00;
                        break;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(
                    "you must set " + KEY_DESIGN_WIDTH + " and " + KEY_DESIGN_HEIGHT + "  in your manifest file.", e);
        }
        L.e(" designWidth =" + mDesignWidth + " , designHeight = " + mDesignHeight + " , mConversion = " + mConversion);
    }
}
