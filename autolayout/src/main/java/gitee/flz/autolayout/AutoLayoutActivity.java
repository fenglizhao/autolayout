package gitee.flz.autolayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Modified to by flz on 20/02/26.
 */
public class AutoLayoutActivity extends AppCompatActivity
{
    private static final String LAYOUT_LINEARLAYOUT = "LinearLayout";
    private static final String LAYOUT_FRAMELAYOUT = "FrameLayout";
    private static final String LAYOUT_RELATIVELAYOUT = "RelativeLayout";
    private static final String LAYOUT_SCROLLVIEW = "ScrollView";
    private static final String LAYOUT_RADIOGROUP = "RadioGroup";
    private static final String LAYOUT_TABLELAYOUT = "TableLayout";
    private static final String LAYOUT_TABLEROW = "TableRow";
    private static final String LAYOUT_SWIPEREFRESHLAYOUT = "SwipeRefreshLayout";
    private static final String LAYOUT_CONSTRAINTLAYOUT = "ConstraintLayout";

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs)
    {
        View view = null;
        if (name.equals(LAYOUT_FRAMELAYOUT)) {
            view = new AutoFrameLayout(context, attrs);
        }

        if (name.equals(LAYOUT_LINEARLAYOUT)) {
            view = new AutoLinearLayout(context, attrs);
        }

        if (name.equals(LAYOUT_RELATIVELAYOUT)) {
            view = new AutoRelativeLayout(context, attrs);
        }

        if (name.equals(LAYOUT_SCROLLVIEW)) {
            view = new AutoScrollView(context, attrs);
        }

        if (name.equals(LAYOUT_RADIOGROUP)) {
            view = new AutoRadioGroup(context, attrs);
        }

        if (name.equals(LAYOUT_TABLELAYOUT)) {
            view = new AutoTableLayout(context, attrs);
        }

        if (name.equals(LAYOUT_TABLEROW)) {
            view = new AutoTableRow(context, attrs);
        }

        if (name.contains(".")) {
            String str = name.substring(name.lastIndexOf(".")+1);
            if (str.equals(LAYOUT_SWIPEREFRESHLAYOUT)) {
                view = new AutoSwipeRefreshLayout(context, attrs);
            }

            if (str.equals(LAYOUT_CONSTRAINTLAYOUT)) {
                view = new AutoConstraintlayout(context, attrs);
            }
        }

        if (view != null) return view;

        return super.onCreateView(name, context, attrs);
    }
}
